package com.example.saianushapenujuri.assignment_kotlin

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_explicitintent.*

class ExplicitIntentActivity :AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explicitintent)

        btn_click.setOnClickListener {

            var message:String=edit_message.text.toString();
            var intent= Intent(this,ExplicitNextActivity::class.java)
            intent.putExtra("MESSAGE",message)
            startActivity(intent)
        }
    }
}