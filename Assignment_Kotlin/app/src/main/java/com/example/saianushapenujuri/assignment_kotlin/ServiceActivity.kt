package com.example.saianushapenujuri.assignment_kotlin

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_service.*

class ServiceActivity:AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)

        buttonStart.setOnClickListener {
            startService(Intent(this, MyService::class.java))

        }

        buttonStop.setOnClickListener {
            stopService(Intent(this, MyService::class.java))

        }
        buttonNext.setOnClickListener {
            val intent = Intent(this, ServiceSecondActivity::class.java)
            startActivity(intent)
        }

    }
}