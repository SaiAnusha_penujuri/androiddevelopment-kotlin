package com.example.saianushapenujuri.assignment_kotlin

import android.content.ContentValues
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_sqlite.*

class SQLiteActivity:AppCompatActivity(){
    private var helper: DbHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sqlite)

        helper = DbHelper(this,"mydb",null,1)

        btn_insert.setOnClickListener {
            insert();
        }
        btn_display.setOnClickListener {
            display();
        }
        btn_update.setOnClickListener {
            update();
        }
        btn_delete.setOnClickListener {
            delete();
        }

    }

    fun insert() {
        val db = helper!!.getWritableDatabase()
        val values = ContentValues()
        values.put("name", getName())
        values.put("age", getAge())
        db.insert("Stu_Table", null, values)
        Toast.makeText(applicationContext, "data inserted", Toast.LENGTH_SHORT).show()
        db.close()
    }

    fun display() {
        text_info.setText(" ")
        val db = helper!!.getReadableDatabase()
        val table = "stu_table"
        val columns: Array<String>? = null
        val selection: String? = null
        val selectionArgs: Array<String>? = null
        val groupBy: String? = null
        val having: String? = null
        val orderBy: String? = null
        val cursor = db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy)

        val buffer = StringBuffer()
        while (cursor.moveToNext()) {
            buffer.append("Name :" + cursor.getString(0) + "\n")
            buffer.append("Age :" + cursor.getString(1) + "\n")
        }
        text_info.setText(buffer.toString())
        db.close()

    }

    fun update() {
        val db = helper!!.getWritableDatabase()
        val table = "Stu_Table"
        val values = ContentValues()
        values.put("name", getName())
        val whereClause = "age=?"
        val whereArgs = arrayOf(getAge().toString())
        db.update(table, values, whereClause, whereArgs)
        Toast.makeText(applicationContext, "data updated", Toast.LENGTH_SHORT).show()
        db.close()
    }

    fun delete() {
        val db = helper!!.getWritableDatabase()
        val table = "Stu_Table"
        val whereClause = "age=?"
        val whereArgs = arrayOf(getAge().toString())
        db.delete(table, whereClause, whereArgs)
        Toast.makeText(applicationContext, "data deleted", Toast.LENGTH_SHORT).show()
        db.close()
    }

    fun getName(): String {
        return edit_name.text.toString()
    }

    fun getAge(): Int {
        return Integer.parseInt(edit_age.text.toString())
    }
}