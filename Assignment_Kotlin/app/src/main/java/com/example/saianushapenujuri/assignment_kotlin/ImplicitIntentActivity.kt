package com.example.saianushapenujuri.assignment_kotlin

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_implicitintent.*

class ImplicitIntentActivity:AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_implicitintent)

        btn_implicit.setOnClickListener {

            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.gmail.com"))
            startActivity(intent)
        }
    }
}