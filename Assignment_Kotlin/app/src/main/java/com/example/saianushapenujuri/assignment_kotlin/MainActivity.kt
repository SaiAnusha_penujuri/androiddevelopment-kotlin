package com.example.saianushapenujuri.assignment_kotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_BroadcastReceiver.setOnClickListener {
            var intent= Intent(this,BroadcastReceiverActivity::class.java)
            startActivity(intent)
        }
        btn_ContentProvider.setOnClickListener {
            var intent=Intent(this,ContentProviderActivity::class.java)
            startActivity(intent)
        }
        btn_ExplictIntent.setOnClickListener {
            var intent= Intent(this,ExplicitIntentActivity::class.java)
            startActivity(intent)
        }
        btn_ImplictIntent.setOnClickListener {
            var intent= Intent(this,ImplicitIntentActivity::class.java)
            startActivity(intent)
        }
        btn_Service.setOnClickListener {
            var intent= Intent(this,ServiceActivity::class.java)
            startActivity(intent)
        }
        btn_Share.setOnClickListener {
            var intent= Intent(this,ShareActivity::class.java)
            startActivity(intent)
        }
        btn_sqlite.setOnClickListener {
            var intent=Intent(this,SQLiteActivity::class.java)
            startActivity(intent)
        }
    }
}
