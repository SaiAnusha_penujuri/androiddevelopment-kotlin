package com.example.saianushapenujuri.assignment_kotlin

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.widget.Toast

class MyService : Service() {
    internal var mPlayer: MediaPlayer?=null

    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }

    override fun onCreate() {
        Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show()
        mPlayer = MediaPlayer.create(this, R.raw.ringtone)
        //mPlayer.setLooping(false)

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Toast.makeText(this, "Service started", Toast.LENGTH_LONG).show()
        mPlayer!!.start()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        //super.onDestroy()
        Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show()
        mPlayer!!.stop()

    }
}