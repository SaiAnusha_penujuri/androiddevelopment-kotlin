package com.example.saianushapenujuri.assignment_kotlin

import android.os.Bundle
import android.provider.ContactsContract
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.ListView
import java.util.ArrayList

class ContentProviderActivity:AppCompatActivity(){
    private var mList: ListView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contentprovider)
        mList = findViewById(R.id.list_contact) as ListView
        fetchContent()
    }

    fun fetchContent() {
        var contact_list = ArrayList<String>()
        var uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        var projection = arrayOf(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER)
        var selection: String? = null
        var selectionArgs: Array<String>? = null
        var sortOrder: String? = null
        var resolver = contentResolver
        var cursor = resolver.query(uri, projection, selection, selectionArgs, sortOrder)
        while (cursor.moveToNext()) {
            contact_list.add(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)))
            contact_list.add(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)))
        }
        mList!!.setAdapter(ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contact_list))

    }
}