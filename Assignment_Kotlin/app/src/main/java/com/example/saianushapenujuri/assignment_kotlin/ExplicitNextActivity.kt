package com.example.saianushapenujuri.assignment_kotlin

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_explicitnext.*

class ExplicitNextActivity:AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explicitnext)

        var bundle: Bundle?=intent.extras
        var msg=bundle!!.getString("MESSAGE")
        text_message.text=msg
    }
}