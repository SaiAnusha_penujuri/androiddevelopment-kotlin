package com.example.saianushapenujuri.scan_kotlin

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Toast


class JobCompleteReceiver : BroadcastReceiver() {
    var ACTION_SCAN_COMPLETED = "com.hp.jetadvantage.link.sample.scansample.ACTION_SCAN_COMPLETED"


    override fun onReceive(context: Context, intent: Intent?) {
        if (intent == null) {
            Log.d(TAG, "Received intent is null")
            return
        }

        val action = intent.action
        val component = intent.component
        // Verify that received Job Id is same as expected one
        val jobId = intent.getStringExtra(JOB_ID_EXTRA)
        val expectedJobId = PreferenceManager.getDefaultSharedPreferences(context)
            .getString(ScanConfigureFragment.CURRENT_JOB_ID, null)

        if (ACTION_SCAN_COMPLETED.equals(action) &&
            component != null && context.packageName == component.packageName &&
            jobId == expectedJobId
        ) {
            Log.d(TAG, "Received job completed intent")
            Toast.makeText(context, R.string.received_complete_intent, Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        private val TAG = "[ScanSample]" + JobCompleteReceiver::class.java.simpleName

        val RID_EXTRA = "rid"
        val JOB_ID_EXTRA = "jobid"
    }
}
