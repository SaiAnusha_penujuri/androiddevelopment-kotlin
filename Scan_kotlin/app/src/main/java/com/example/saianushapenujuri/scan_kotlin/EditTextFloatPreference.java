package com.example.saianushapenujuri.scan_kotlin;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.preference.EditTextPreference;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.AttributeSet;

import java.lang.ref.WeakReference;

/**
 * Helper {@link EditTextPreference}
 * for convenient numbers input.
 */
public final class EditTextFloatPreference extends EditTextPreference {
    /**
     * Entered integer value
     */
    private Float mFloat;
    /**
     * Minimum value
     */
    private float mMin = 0;
    /**
     * Max value
     */
    private float mMax = 0;
    /**
     * Title string to append min and max to
     */
    private String mTitle = "";

    public EditTextFloatPreference(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditTextFloatPreference(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextFloatPreference(final Context context) {
        super(context);
        init();
    }

    private static Float parseFloat(final String text) {
        if (text != null) {
            try {
                return Float.parseFloat(text);
            } catch (NumberFormatException e) {
                // not float value
            }
        }
        return null;
    }

    /**
     * @return max input value
     */
    public float getMaxVal() {
        return mMax;
    }

    /**
     * Sets max possible input value
     *
     * @param val int
     */
    public void setMaxVal(final float val) {
        mMax = val;
        applyLimits();
    }

    /**
     * @return min input value
     */
    public float getMinVal() {
        return mMin;
    }

    /**
     * Sets min possible input value
     *
     * @param val int
     */
    public void setMinVal(final float val) {
        mMin = val;
        applyLimits();
    }

    /**
     * Sets limits for input ints
     *
     * @param min int
     * @param max int
     */
    public void setLimits(final float min, final float max) {
        mMin = min;
        mMax = max;
        applyLimits();
    }

    /**
     * Sets initial properties of input
     */
    private void init() {
        getEditText().setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        mTitle = (String) getDialogTitle();
        applyLimits();
    }

    /**
     * Applies min and max limits to the input:
     * it's not possible to enter smaller or bigger values.
     */
    private void applyLimits() {
        final InputFilter curFilters[] = getEditText().getFilters();
        final InputFilterMinMax limitsFilter = new InputFilterMinMax(this, mMin, mMax);

        setDialogTitle(mTitle + String.format(getContext().getString(R.string.title_format), mMin, mMax));

        if (curFilters != null) {
            // Check if we have these filter already
            for (int idx = 0; idx < curFilters.length; idx++) {
                if (curFilters[idx] instanceof InputFilterMinMax) {
                    curFilters[idx] = limitsFilter;
                    return;
                }
            }

            // there's no limitsFilter in the list, add it!
            final InputFilter newFilters[] = new InputFilter[curFilters.length + 1];

            System.arraycopy(curFilters, 0, newFilters, 0, curFilters.length);
            newFilters[curFilters.length] = limitsFilter;
            getEditText().setFilters(newFilters);
        } else {
            getEditText().setFilters(new InputFilter[]{limitsFilter});
        }
    }

    @Override
    public String getText() {
        return mFloat != null ? mFloat.toString() : null;
    }

    @Override
    public void setText(final String text) {
        final boolean wasBlocking = shouldDisableDependents();

        mFloat = parseFloat(text);

        persistString(mFloat != null ? mFloat.toString() : null);

        final boolean isBlocking = shouldDisableDependents();

        if (isBlocking != wasBlocking) {
            notifyDependencyChange(isBlocking);
        }
    }

    /**
     * @return {@link Float} currently stored
     */
    public Float getFloat() {
        return mFloat;
    }

    /**
     * Special filter for max and min values
     */
    private static class InputFilterMinMax implements InputFilter {
        private final float min;
        private final float max;

        /**
         * Owner reference
         */
        private final WeakReference<EditTextPreference> mParentRef;

        /**
         * Default constructor
         *
         * @param parent {@link EditTextPreference}
         * @param min    int
         * @param max    max
         */
        public InputFilterMinMax(final EditTextPreference parent, final float min, final float max) {
            this.min = min;
            this.max = max;
            mParentRef = new WeakReference<>(parent);
        }

        @Override
        public CharSequence filter(final CharSequence source, final int start, final int end, final Spanned dest,
                                   final int dstart, final int dend) {
            boolean inRange = false;

            try {
                // Remove the string out of destination that is to be replaced
                String replacement = source.subSequence(start, end).toString();

                if (!".".equals(replacement)) {
                    // Try convert replacement to integer to validate symbol
                    try {
                        Float.parseFloat(replacement);
                    } catch (NumberFormatException e) {
                        // Not int
                        replacement = "";
                    }
                }

                final String newVal = dest.subSequence(0, dstart).toString() + replacement + dest.subSequence(dend, dest.length()).toString();
                final float input = Float.parseFloat(newVal);

                inRange = isInRange(min, max, input);
            } catch (NumberFormatException ignored) {
            }

            final EditTextPreference parent = mParentRef.get();

            if (parent != null) {
                final Dialog dialog = parent.getDialog();

                if (dialog instanceof AlertDialog) {
                    ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(inRange);
                }
            }

            return null;
        }

        /**
         * Helper to check if number is in range
         *
         * @return true if c is in range [a, b]
         */
        private boolean isInRange(final float a, final float b, final float c) {
            return Float.compare(b, a) > 0 ? Float.compare(c, a) >= 0 && Float.compare(c, b) <= 0 : Float.compare(c, b) >= 0 && Float.compare(c, a) <= 0;
        }
    }
}