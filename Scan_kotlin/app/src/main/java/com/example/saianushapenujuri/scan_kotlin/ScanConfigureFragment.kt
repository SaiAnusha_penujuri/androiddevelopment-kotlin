package com.example.saianushapenujuri.scan_kotlin

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.preference.*
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import com.hp.jetadvantage.link.api.CapabilitiesExceededException
import com.hp.jetadvantage.link.api.Result
import com.hp.jetadvantage.link.api.SsdkUnsupportedException
import com.hp.jetadvantage.link.api.callback.Callback
import com.hp.jetadvantage.link.api.callback.CallbackAttributes
import com.hp.jetadvantage.link.api.callback.CallbackService
import com.hp.jetadvantage.link.api.job.JobInfo
import com.hp.jetadvantage.link.api.job.JobService
import com.hp.jetadvantage.link.api.job.ScanJobData
import com.hp.jetadvantage.link.api.scanner.*
import com.hp.jetadvantage.link.api.scanner.ScanAttributes.*
import kotlinx.android.synthetic.main.fragment_scanconfigure.view.*
import org.apache.commons.io.IOUtils
import java.io.IOException
import java.security.*
import java.security.spec.InvalidKeySpecException
import java.security.spec.PKCS8EncodedKeySpec
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ScanConfigureFragment : PreferenceFragment(), SharedPreferences.OnSharedPreferenceChangeListener {

    var caps: ScanAttributesCaps? = null

    companion object {
        val PREF_FILE_NAME = "pref_filename"
        val PREF_FOLDER_NAME = "pref_folder_name"
        val PREF_COLOR_MODE = "pref_colorMode"
        val PREF_DUPLEX_MODE = "pref_duplexMode"
        val PREF_RESOLUTION_TYPE = "pref_resolutionType"
        val PREF_DOC_FORMAT = "pref_docFormat"
        val PREF_ORG_SIZE = "pref_originalSize"
        val PREF_CUSTOM_LENGTH = "pref_customLength"
        val PREF_CUSTOM_WIDTH = "pref_customWidth"
        val PREF_ORIENTATION = "pref_orientation"
        val PREF_SCAN_PREVIEW = "pref_scanPreview"
        val PREF_BACKGROUND_CLEANUP = "pref_backgroundCleanup"
        val PREF_CONTRAST_ADJUSTMENT = "pref_contrastAdjustment"
        val PREF_DARKNESS_ADJUSTMENT = "pref_darknessAdjustment"
        val PREF_BLACK_IMAGE_REMOVAL_MODE = "pref_blankImageRemovalMode"
        val PREF_COLOR_DROPOUT_MODE = "pref_colorDropoutMode"
        val PREF_CROP_MODE = "pref_cropMode"
        val PREF_OUTPUT_QUALITY = "pref_outputQuality"
        val PREF_TRANSMISSION_MODE = "pref_transmissionMode"
        val PREF_SHARPNESS_ADJUSTMENT = "pref_sharpnessAdjustment"
        val PREF_MEDIA_WEIGHT_ADJUSTMENT = "pref_mediaWeightAdjustment"
        val PREF_TEXT_PHOTO_OPTIMIZATION = "pref_textPhotoOptimization"
        val PREF_MEDIA_SOURCE = "pref_mediaSource"
        val PREF_MISFEED_DETECTION_MODE = "pref_misfeedDetectionMode"
        val PREF_PDF_COMPRESSION = "pref_pdf_compression"
        val PREF_OCR_LANGUAGE = "pref_ocr_language"
        val PREF_PDF_PASSWORD = "pref_pdf_password"
        val PREF_TIFF_COMPRESSION = "pref_tiff_compression"
        val PREF_XPS_COMPRESSION = "pref_xps_compression"

        val PREF_BASE_ATTRIBUTES_CATEGORY = "base_attributes_category"
        val CURRENT_JOB_ID = "pref_currentJobId"

        private val TAG = "[ScanSample]" + ScanConfigureFragment::class.java!!.simpleName
    }

    private var mBaseAttributesCategory: PreferenceCategory? = null

    private var mFilenamePref: EditTextPreference?=null
    private var mFoldernamePref: FolderPathPreference? = null

    private var mTransmissionPref: ListPreference? = null
    private var mPDFCompressionPref: ListPreference? = null
    private var mOCRLanguagePref: ListPreference? = null
    private var mPDFPasswordPref: EditTextPreference? = null
    private var mTIFFCompressionPref: ListPreference? = null
    private var mXPSCompressionPref: ListPreference? = null
    private var mCustomLengthPref: EditTextFloatPreference? = null
    private var mCustomWidthPref: EditTextFloatPreference? = null

    private var mCapabilities: ScanAttributesCaps? = null
    private var mFileOptionsAttributesCaps: FileOptionsAttributesCaps? = null
    private var mPrefs: SharedPreferences? = null
    private var mJobId: String? = null
    private var mJobObserver: JobObserver? = null
    private var activity: MainActivity? = null
    var isSDKInitialized: Boolean = false
    private lateinit var mButtonScan: ImageButton
    private var mProgressDialog: ProgressDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        addPreferencesFromResource(R.xml.scan_preferences)
        val view = inflater.inflate(R.layout.fragment_scanconfigure, container, false)
        initializeFragment(view)
        return view
    }

    private fun initializeFragment(view: View) {
        mButtonScan = view.scan_imageButton
        mButtonScan.setOnClickListener {
            scanToDestination()
        }
        mJobObserver = JobObserver(Handler())
        activity = MainActivity()

        mFilenamePref = findPreference(PREF_FILE_NAME) as EditTextPreference
        mFilenamePref!!.text = null
        mFoldernamePref = findPreference(PREF_FOLDER_NAME) as FolderPathPreference
        mFoldernamePref!!.text = null

        mTransmissionPref = findPreference(PREF_TRANSMISSION_MODE) as ListPreference
        mPDFCompressionPref = findPreference(PREF_PDF_COMPRESSION) as ListPreference
        mOCRLanguagePref = findPreference(PREF_OCR_LANGUAGE) as ListPreference
        mPDFPasswordPref = findPreference(PREF_PDF_PASSWORD) as EditTextPreference
        mTIFFCompressionPref = findPreference(PREF_TIFF_COMPRESSION) as ListPreference
        mXPSCompressionPref = findPreference(PREF_XPS_COMPRESSION) as ListPreference
        mCustomLengthPref = findPreference(PREF_CUSTOM_LENGTH) as EditTextFloatPreference
        mCustomLengthPref!!.setLimits(0F, 0F)
        mCustomLengthPref!!.text = null
        mCustomWidthPref = findPreference(PREF_CUSTOM_WIDTH) as EditTextFloatPreference
        mCustomWidthPref!!.setLimits(0F, 0F)
        mCustomWidthPref!!.text = null
        mBaseAttributesCategory = findPreference(PREF_BASE_ATTRIBUTES_CATEGORY) as PreferenceCategory

    }

    override fun onResume() {
        super.onResume()

        mJobObserver!!.register(getActivity())
        handleComplete()
        val result = Result()
        val caps = requestCaps(getActivity(), result)
        loadCapabilities(caps)
        var prefs = preferenceScreen.sharedPreferences as SharedPreferences
        prefs.registerOnSharedPreferenceChangeListener(this)
        refreshAllPrefs(prefs)
    }

    override fun onPause() {
        super.onPause()

        mJobObserver!!.unregister(getActivity())
        val prefs = preferenceScreen.sharedPreferences
        prefs.unregisterOnSharedPreferenceChangeListener(this)
    }

    fun handleException(e: Exception) {
        val errorMsg: String

        Log.e(TAG, e.message)

        if (e is SsdkUnsupportedException) {
            when ((e as SsdkUnsupportedException).type) {
                SsdkUnsupportedException.LIBRARY_NOT_INSTALLED, SsdkUnsupportedException.LIBRARY_UPDATE_IS_REQUIRED -> errorMsg =
                        getString(R.string.sdk_support_missing)
                else -> errorMsg = getString(R.string.unknown_error)
            }
        } else {
            errorMsg = e.message!!
        }

        AlertDialog.Builder(getActivity())
            .setTitle("Error")
            .setMessage(errorMsg)
            .setCancelable(false)
            .setPositiveButton(
                android.R.string.ok
            ) { dialog, which -> getActivity().finish() }
            .show()
    }

    fun showToast(text: String) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show()
    }

    fun handleComplete() {
        isSDKInitialized = true
        val attributes = CallbackAttributes.Builder()
            .setSigningCallback(ScanEncryptCallback(getActivity()))
            .build()
        CallbackService.register(getActivity(), attributes)

    }

    internal class ScanEncryptCallback(context: Context) : Callback.SigningCallback {
        private val mPrivateKey: PrivateKey?

        init {
            var privateKey: PrivateKey? = null
            try {
                val secret = IOUtils.toByteArray(context.assets.open("certificate.pkcs8"))

                val pkcs8EncodedKeySpec = PKCS8EncodedKeySpec(secret)
                privateKey = KeyFactory.getInstance("RSA").generatePrivate(pkcs8EncodedKeySpec)
            } catch (e: IOException) {
                Log.e(TAG, "Error loading private key", e)
            } catch (e: NoSuchAlgorithmException) {
                Log.e(TAG, "Failed to get private key", e)
            } catch (e: InvalidKeySpecException) {
                Log.e(TAG, "Failed to get private key", e)
            }

            mPrivateKey = privateKey
        }

        override fun sign(signature: Signature, data: ByteArray): ByteArray? {
            var signData: ByteArray? = null
            if (mPrivateKey != null) {
                Log.d(TAG, "Encrypting data with private key")
                try {
                    signature.initSign(mPrivateKey)
                    signature.update(data)
                    signData = signature.sign()
                } catch (e: InvalidKeyException) {
                    Log.e(TAG, "Failed to sign data with private key", e)
                } catch (e: SignatureException) {
                    Log.e(TAG, "Failed to sign data with private key", e)
                }

            } else {
                Log.w(TAG, "Failed to sign data - private key is null")
            }

            return signData
        }
    }

    fun scanToDestination() {
        mProgressDialog = ProgressDialog(getActivity())
        mProgressDialog!!.setMessage("Scanning documents")
        mProgressDialog!!.show()

        mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity())
        try {
            val settingsUi = false
            Log.d(TAG, "Settings UI:" + false)

            val attributes: ScanAttributes
            val caps = mCapabilities

            val dest = ScanAttributes.Destination.valueOf(ScanAttributes.Destination.ME.name)

            attributes = buildScanAttributes(dest, caps)

            val taskAttribs = ScanletAttributes.Builder()
                .setShowSettingsUi(settingsUi)
                .build()

            // Submit the job
            val rid = ScannerService.submit(getActivity(), attributes, taskAttribs)
            Log.i(TAG, "Job submitted with rid = $rid")
        } catch (e: IllegalArgumentException) {
            Log.e(TAG, "Illegal argument was provided: ", e)
        } catch (e: CapabilitiesExceededException) {
            Log.e(TAG, "Caps were exceeded: ", e)
        }

    }

    @Throws(CapabilitiesExceededException::class)
    private fun buildScanAttributes(
        dest: ScanAttributes.Destination,
        capabilities: ScanAttributesCaps?
    ): ScanAttributes {
        val df = ScanAttributes.DocumentFormat.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_DOC_FORMAT,
                ScanAttributes.DocumentFormat.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected Doc Format:$df")

        val cm = ScanAttributes.ColorMode.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_COLOR_MODE,
                ScanAttributes.ColorMode.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected Color Mode:" + cm.name)

        val du = ScanAttributes.Duplex.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_DUPLEX_MODE,
                ScanAttributes.Duplex.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected Duplex Mode:" + du.name)

        val orientation = ScanAttributes.Orientation.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_ORIENTATION,
                ScanAttributes.Orientation.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected Orientation:" + orientation.name)

        val ss = ScanAttributes.ScanSize.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_ORG_SIZE,
                ScanAttributes.ScanSize.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected Scan Size:" + ss.name)

        val customLength = java.lang.Float.valueOf(mPrefs!!.getString(ScanConfigureFragment.PREF_CUSTOM_LENGTH, "0")!!)
        Log.d(TAG, "Selected Custom Length:$customLength")

        val customWidth = java.lang.Float.valueOf(mPrefs!!.getString(ScanConfigureFragment.PREF_CUSTOM_WIDTH, "0")!!)
        Log.d(TAG, "Selected Custom Width:$customWidth")

        val resolution = ScanAttributes.Resolution.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_RESOLUTION_TYPE,
                ScanAttributes.Resolution.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected Resolution:" + resolution.name)

        val backgroundCleanup = ScanAttributes.BackgroundCleanup.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_BACKGROUND_CLEANUP,
                ScanAttributes.BackgroundCleanup.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected BackgroundCleanup:" + backgroundCleanup.name)

        val contrastAdjustment = ScanAttributes.ContrastAdjustment.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_CONTRAST_ADJUSTMENT,
                ScanAttributes.ContrastAdjustment.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected ContrastAdjustment:" + contrastAdjustment.name)

        val darknessAdjustment = ScanAttributes.DarknessAdjustment.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_DARKNESS_ADJUSTMENT,
                ScanAttributes.DarknessAdjustment.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected DarknessAdjustment:" + darknessAdjustment.name)

        val blankImageRemovalMode = ScanAttributes.BlankImageRemovalMode.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_BLACK_IMAGE_REMOVAL_MODE,
                ScanAttributes.BlankImageRemovalMode.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected BlankImageRemovalMode:" + blankImageRemovalMode.name)

        val colorDropoutMode = ScanAttributes.ColorDropoutMode.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_COLOR_DROPOUT_MODE,
                ScanAttributes.ColorDropoutMode.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected ColorDropoutMode:" + colorDropoutMode.name)

        val cropMode = ScanAttributes.CropMode.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_CROP_MODE,
                ScanAttributes.CropMode.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected CropMode:" + cropMode.name)

        val outputQuality = ScanAttributes.OutputQuality.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_OUTPUT_QUALITY,
                ScanAttributes.OutputQuality.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected OutputQuality:" + outputQuality.name)

        val transmissionMode = ScanAttributes.TransmissionMode.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_TRANSMISSION_MODE,
                ScanAttributes.TransmissionMode.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected TransmissionMode:" + transmissionMode.name)

        val scanPreview = ScanAttributes.ScanPreview.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_SCAN_PREVIEW,
                ScanAttributes.ScanPreview.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected ScanPreview:" + scanPreview.name)

        val sharpnessAdjustment = ScanAttributes.SharpnessAdjustment.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_SHARPNESS_ADJUSTMENT,
                ScanAttributes.SharpnessAdjustment.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected SharpnessAdjustment:" + sharpnessAdjustment.name)

        val mediaWeightAdjustment = ScanAttributes.MediaWeightAdjustment.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_MEDIA_WEIGHT_ADJUSTMENT,
                ScanAttributes.MediaWeightAdjustment.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected MediaWeightAdjustment:" + mediaWeightAdjustment.name)

        val textPhotoOptimization = ScanAttributes.TextPhotoOptimization.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_TEXT_PHOTO_OPTIMIZATION,
                ScanAttributes.TextPhotoOptimization.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected TextPhotoOptimization:" + textPhotoOptimization.name)

        val mediaSource = ScanAttributes.MediaSource.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_MEDIA_SOURCE,
                ScanAttributes.MediaSource.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected MediaSource:" + mediaSource.name)

        val misfeedDetectionMode = ScanAttributes.MisfeedDetectionMode.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_MISFEED_DETECTION_MODE,
                ScanAttributes.MisfeedDetectionMode.DEFAULT.name
            )
        )
        Log.d(TAG, "Selected MisfeedDetectionMode:" + misfeedDetectionMode.name)

        val pdfEncryption = mPrefs!!.getString(ScanConfigureFragment.PREF_PDF_PASSWORD, null)

        val ocrLanguage = FileOptionsAttributes.OcrLanguage.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_OCR_LANGUAGE,
                FileOptionsAttributes.OcrLanguage.DEFAULT.name
            )
        )

        val compressionMode = FileOptionsAttributes.PdfCompressionMode.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_PDF_COMPRESSION,
                FileOptionsAttributes.PdfCompressionMode.DEFAULT.name
            )
        )

        val tiffCompressionMode = FileOptionsAttributes.TiffCompressionMode.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_TIFF_COMPRESSION,
                FileOptionsAttributes.TiffCompressionMode.DEFAULT.name
            )
        )

        val xpsCompressionMode = FileOptionsAttributes.XpsCompressionMode.valueOf(
            mPrefs!!.getString(
                ScanConfigureFragment.PREF_XPS_COMPRESSION,
                FileOptionsAttributes.XpsCompressionMode.DEFAULT.name
            )
        )

        val filename = mPrefs!!.getString(ScanConfigureFragment.PREF_FILE_NAME, null)
        val foldername = mPrefs!!.getString(ScanConfigureFragment.PREF_FOLDER_NAME, null)

        val fileOptionsAttrCaps = mFileOptionsAttributesCaps

        Log.d(
            TAG, "FileOptionsAttributes option\npdfEncryption: " + pdfEncryption + "\n"
                    + "ocrLanguage: " + ocrLanguage.name + "\n"
                    + "compressionMode: " + compressionMode.name + "\n"
                    + "tiffCompressionMode: " + tiffCompressionMode.name + "\n"
                    + "xpsCompressionMode: " + xpsCompressionMode.name + "\n"
        )

        val fileOptionsAttributes = FileOptionsAttributes.Builder()
            .setPdfEncryptionPassword(pdfEncryption)
            .setOcrLanguage(ocrLanguage)
            .setPdfCompressionMode(compressionMode)
            .setTiffCompressionMode(tiffCompressionMode)
            .setXpsCompressionMode(xpsCompressionMode)
            .build(fileOptionsAttrCaps!!)

        return ScanAttributes.MeBuilder()
            .setColorMode(cm)
            .setDuplex(du)
            .setDocumentFormat(df)
            .setScanSize(ss)
            .setCustomLength(customLength)
            .setCustomWidth(customWidth)
            .setResolution(resolution)
            .setOrientation(orientation)
            .setScanPreview(scanPreview)
            .setBackgroundCleanup(backgroundCleanup)
            .setContrastAdjustment(contrastAdjustment)
            .setDarknessAdjustment(darknessAdjustment)
            .setBlankImageRemovalMode(blankImageRemovalMode)
            .setColorDropoutMode(colorDropoutMode)
            .setCropMode(cropMode)
            .setOutputQuality(outputQuality)
            .setTransmissionMode(transmissionMode)
            .setSharpnessAdjustment(sharpnessAdjustment)
            .setMediaWeightAdjustment(mediaWeightAdjustment)
            .setTextPhotoOptimization(textPhotoOptimization)
            .setMediaSource(mediaSource)
            .setMisfeedDetectionMode(misfeedDetectionMode)
            .setFileOptionsAttributes(fileOptionsAttributes)
            .setFolderName(foldername)
            .setFileName(filename)
            .build(capabilities!!)

    }

    fun requestCaps(context: Context, result: Result?): ScanAttributesCaps? {
        var result = result
        if (result == null) {
            result = Result()
        }
        mCapabilities = ScannerService.getCapabilities(context, result)
        if (result.code == Result.RESULT_OK && mCapabilities != null) {
            Log.d(
                TAG,
                "Received Caps as:" +
                        " Destination:" + mCapabilities!!.destinationList +
                        ",ColorMode:" + mCapabilities!!.colorModeList.toString() +
                        ",Duplex:" + mCapabilities!!.duplexList.toString() +
                        ",DocFormat(Me):" + mCapabilities!!.getDocumentFormatList(ScanAttributes.Destination.ME).toString() +
                        ",Orientation:" + mCapabilities!!.orientationList +
                        ",Resolution:" + mCapabilities!!.resolutionList +
                        ",ScanPreview:" + mCapabilities!!.scanPreviewList +
                        ",ScanSize:" + mCapabilities!!.scanSizeList +
                        ",CustomLength:" + mCapabilities!!.customLengthRange +
                        ",CustomWidth:" + mCapabilities!!.customWidthRange +
                        ",BackgroundCleanup:" + mCapabilities!!.backgroundCleanupList +
                        ",ContrastAdjustment:" + mCapabilities!!.contrastAdjustmentList +
                        ",DarknessAdjustment:" + mCapabilities!!.darknessAdjustmentList +
                        ",SharpnessAdjustment:" + mCapabilities!!.sharpnessAdjustmentList +
                        ",BlankImageRemovalMode:" + mCapabilities!!.blankImageRemovalModeList +
                        ",ColorDropoutMode:" + mCapabilities!!.colorDropoutModeList +
                        ",CropMode:" + mCapabilities!!.cropModeList +
                        ",OutputQuality:" + mCapabilities!!.outputQualityList +
                        ",TransmissionMode:" + mCapabilities!!.transmissionModeList +
                        ",MediaWeightAdjustment:" + mCapabilities!!.mediaWeightAdjustmentList +
                        ",TextPhotoOptimization:" + mCapabilities!!.textPhotoOptimizationList +
                        ",MediaSource:" + mCapabilities!!.mediaSourceList +
                        ",MisfeedDetectionMode:" + mCapabilities!!.misfeedDetectionModeList
            )

            for (entry in mCapabilities!!.documentFormatsByColorMode.entries) {
                Log.d(TAG, "getDocumentFormatsByColorMode: " + entry.value + " [" + entry.key + "]")
            }

            val defaultAttr = ScannerService.getDefaults(context, result)
            if (result!!.code == Result.RESULT_OK && defaultAttr != null) {
                Log.d(TAG, "defaultAttr (default): " + defaultAttr!!.toString())
            } else {
                Log.e(TAG, result!!.toString())
            }

            if (ScannerStatus.isSupported(context)) {
                val statusInfo = ScannerStatus.getStatus(context, result)
                if (result!!.code == Result.RESULT_OK) {
                    Log.d(TAG, "StatusInfo: $statusInfo")
                } else {
                    Log.e(TAG, result!!.toString())
                }
            } else {
                Log.e(TAG, "ScannerStatus is not supported")
            }

            // get file options for defaults
            mFileOptionsAttributesCaps = requestFileOptionsCapabilities(
                ScanAttributes.ColorMode.DEFAULT, ScanAttributes.DocumentFormat.DEFAULT
            )
        } else {
            Log.e(TAG, result!!.toString())
        }

        return mCapabilities
    }

    fun requestFileOptionsCapabilities(
        colorMode: ScanAttributes.ColorMode,
        docFormat: ScanAttributes.DocumentFormat
    ): FileOptionsAttributesCaps? {
        mFileOptionsAttributesCaps =
                ScannerService.getFileOptionsCapabilities(getActivity(), colorMode, docFormat, null)
        return mFileOptionsAttributesCaps
    }

    private inner class JobObserver(handler: Handler) : JobService.AbstractJobletObserver(handler) {

        override fun onProgress(rid: String, jobInfo: JobInfo) {
            val bundle: Bundle
            mJobId = Integer.toString(id)
            Log.d(TAG, "Received onProgress for rid $rid")
            Log.d(TAG, "Received onProgress jobInfo $jobInfo")
            if (mJobId == null) {
                if (jobInfo.jobId != null) {
                    mJobId = jobInfo.jobId

                    Log.d(TAG, "Received jobID as " + mJobId!!)
                    showToast(getString(R.string.job_id, mJobId))

                    val prefs = PreferenceManager.getDefaultSharedPreferences(getActivity())
                }
            }
        }

        override fun onComplete(rid: String, jobInfo: JobInfo) {
            Log.d(TAG, "Received onComplete for rid $rid")
            Log.d(TAG, "Received onComplete jobInfo $jobInfo")
            mProgressDialog!!.cancel()

            val scanJobData = jobInfo.getJobData<ScanJobData>()
            val images = scanJobData.fileNames

            if (images != null && images!!.size > 0) {
                showToast("Images " + Arrays.toString(images!!.toTypedArray()))
                Log.d(TAG, "Images: " + Arrays.toString(images!!.toTypedArray()))
            } else {
                showToast(getString(R.string.job_completed))
            }
        }

        override fun onFail(rid: String, result: Result) {
            Log.e(TAG, "Received onFail for rid $rid, $result")

            showToast(getString(R.string.job_failed, result.toString()))
            mProgressDialog!!.cancel()
        }

        override fun onCancel(rid: String) {
            Log.d(TAG, "Received onCancel for rid $rid")
            showToast(getString(R.string.job_cancelled))
            mProgressDialog!!.cancel()
        }
    }

    private fun refreshAllPrefs(prefs: SharedPreferences) {
        onSharedPreferenceChanged(prefs, PREF_FILE_NAME)
        onSharedPreferenceChanged(prefs, PREF_FOLDER_NAME)
        onSharedPreferenceChanged(prefs, PREF_COLOR_MODE)
        onSharedPreferenceChanged(prefs, PREF_DUPLEX_MODE)
        onSharedPreferenceChanged(prefs, PREF_ORIENTATION)
        onSharedPreferenceChanged(prefs, PREF_RESOLUTION_TYPE)
        onSharedPreferenceChanged(prefs, PREF_ORG_SIZE)
        onSharedPreferenceChanged(prefs, PREF_CUSTOM_LENGTH)
        onSharedPreferenceChanged(prefs, PREF_CUSTOM_WIDTH)
        onSharedPreferenceChanged(prefs, PREF_DOC_FORMAT)
        onSharedPreferenceChanged(prefs, PREF_SCAN_PREVIEW)
        onSharedPreferenceChanged(prefs, PREF_BACKGROUND_CLEANUP)
        onSharedPreferenceChanged(prefs, PREF_CONTRAST_ADJUSTMENT)
        onSharedPreferenceChanged(prefs, PREF_DARKNESS_ADJUSTMENT)
        onSharedPreferenceChanged(prefs, PREF_BLACK_IMAGE_REMOVAL_MODE)
        onSharedPreferenceChanged(prefs, PREF_COLOR_DROPOUT_MODE)
        onSharedPreferenceChanged(prefs, PREF_CROP_MODE)
        onSharedPreferenceChanged(prefs, PREF_OUTPUT_QUALITY)
        onSharedPreferenceChanged(prefs, PREF_TRANSMISSION_MODE)
        onSharedPreferenceChanged(prefs, PREF_SHARPNESS_ADJUSTMENT)
        onSharedPreferenceChanged(prefs, PREF_MEDIA_WEIGHT_ADJUSTMENT)
        onSharedPreferenceChanged(prefs, PREF_TEXT_PHOTO_OPTIMIZATION)
        onSharedPreferenceChanged(prefs, PREF_MEDIA_SOURCE)
        onSharedPreferenceChanged(prefs, PREF_MISFEED_DETECTION_MODE)

        onSharedPreferenceChanged(prefs, PREF_PDF_COMPRESSION)
        onSharedPreferenceChanged(prefs, PREF_OCR_LANGUAGE)
        onSharedPreferenceChanged(prefs, PREF_PDF_PASSWORD)
        onSharedPreferenceChanged(prefs, PREF_TIFF_COMPRESSION)
        onSharedPreferenceChanged(prefs, PREF_XPS_COMPRESSION)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        val preference = findPreference(key)

        if (PREF_DOC_FORMAT == key) {
            setColorMode(sharedPreferences, key)
            showOptionalPreference(sharedPreferences, key)
            fillFileOptionAttrCaps()
        } else if (PREF_ORG_SIZE == key) {
            showCustomSizePreference(sharedPreferences, key)
        } else if (PREF_COLOR_MODE == key) {
            fillFileOptionAttrCaps()
        } else if (key == "ME") {
            val entryStr = (preference as ListPreference).entry as String
            val destination =
                if (entryStr == null) ScanAttributes.Destination.ME else ScanAttributes.Destination.valueOf(entryStr)

            fillDocFormatPreferences(caps, PREF_DOC_FORMAT, destination)
            supportTransmissionModeCaps(false)
            mBaseAttributesCategory!!.addPreference(mFoldernamePref)
        }

        if (preference is ListPreference) {
            val entry = (preference as ListPreference).entry as String

            if (entry == null || entry!!.length == 0) {
                (preference as ListPreference).setValueIndex(0)
                preference.setSummary("%s")
            } else {
                preference.setSummary(entry)
            }
        } else if (preference is EditTextPreference) {
            val edit = (preference as EditTextPreference).editText
            var text: String? = (preference as EditTextPreference).text
            if (text != null && edit.transformationMethod != null) {
                text = edit.transformationMethod.getTransformation(text, edit).toString()
            }
            preference.setSummary(text)
        }
    }

    fun loadCapabilities(caps: ScanAttributesCaps?) {
        this.caps = caps

        // Resolution - stored high to low
        var pref: ListPreference

        // Load Resolutions
        pref = findPreference(PREF_RESOLUTION_TYPE) as ListPreference
        val resEntries = ArrayList<CharSequence>()
        val resEntryValues = ArrayList<CharSequence>()

        for (res in caps!!.resolutionList) {
            resEntries.add(res.name)
            resEntryValues.add(res.name)
        }

        pref.entries = resEntries.toTypedArray()
        pref.entryValues = resEntryValues.toTypedArray()
        pref.setDefaultValue(Resolution.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        // Load color mode
        pref = findPreference(PREF_COLOR_MODE) as ListPreference
        val cmEntries = ArrayList<CharSequence>()
        val cmEntryValues = ArrayList<CharSequence>()

        for (cm in caps!!.colorModeList) {
            cmEntries.add(cm.name)
            cmEntryValues.add(cm.name)
        }

        pref.entries = cmEntries.toTypedArray()
        pref.entryValues = cmEntryValues.toTypedArray()
        pref.setDefaultValue(ColorMode.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        // Load duplex mode
        pref = findPreference(PREF_DUPLEX_MODE) as ListPreference
        val duEntries = ArrayList<CharSequence>()
        val duEntryValues = ArrayList<CharSequence>()

        for (du in caps!!.duplexList) {
            duEntries.add(du.name)
            duEntryValues.add(du.name)
        }

        pref.entries = duEntries.toTypedArray()
        pref.entryValues = duEntryValues.toTypedArray()
        pref.setDefaultValue(Duplex.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        //Load Original Orientation
        pref = findPreference(PREF_ORIENTATION) as ListPreference
        val orientationEntries = ArrayList<CharSequence>()
        val orientationEntryValues = ArrayList<CharSequence>()

        for (ori in caps!!.orientationList) {
            orientationEntries.add(ori.name) //name
            orientationEntryValues.add(ori.name) //value
        }
        pref.entries = orientationEntries.toTypedArray()
        pref.entryValues = orientationEntryValues.toTypedArray()
        pref.setDefaultValue(Orientation.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_BACKGROUND_CLEANUP) as ListPreference
        val backgroundCleanupEntries = ArrayList<CharSequence>()
        val backgroundCleanupValues = ArrayList<CharSequence>()

        for (backgroundCleanup in caps!!.backgroundCleanupList) {
            backgroundCleanupEntries.add(backgroundCleanup.name) //name
            backgroundCleanupValues.add(backgroundCleanup.name) //value
        }
        pref.entries = backgroundCleanupEntries.toTypedArray()
        pref.entryValues = backgroundCleanupValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.BackgroundCleanup.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_CONTRAST_ADJUSTMENT) as ListPreference
        val contrastAdjustmentEntries = ArrayList<CharSequence>()
        val contrastAdjustmentValues = ArrayList<CharSequence>()

        for (contrastAdjustment in caps!!.contrastAdjustmentList) {
            contrastAdjustmentEntries.add(contrastAdjustment.name) //name
            contrastAdjustmentValues.add(contrastAdjustment.name) //value
        }
        pref.entries = contrastAdjustmentEntries.toTypedArray()
        pref.entryValues = contrastAdjustmentValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.ContrastAdjustment.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_DARKNESS_ADJUSTMENT) as ListPreference
        val darknessAdjustmentEntries = ArrayList<CharSequence>()
        val darknessAdjustmentValues = ArrayList<CharSequence>()

        for (darknessAdjustment in caps!!.darknessAdjustmentList) {
            darknessAdjustmentEntries.add(darknessAdjustment.name) //name
            darknessAdjustmentValues.add(darknessAdjustment.name) //value
        }
        pref.entries = darknessAdjustmentEntries.toTypedArray()
        pref.entryValues = darknessAdjustmentValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.DarknessAdjustment.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_BLACK_IMAGE_REMOVAL_MODE) as ListPreference
        val blackImageRemovalEntries = ArrayList<CharSequence>()
        val blackImageRemovalValues = ArrayList<CharSequence>()

        for (blankImageRemovalMode in caps!!.blankImageRemovalModeList) {
            blackImageRemovalEntries.add(blankImageRemovalMode.name) //name
            blackImageRemovalValues.add(blankImageRemovalMode.name) //value
        }
        pref.entries = blackImageRemovalEntries.toTypedArray()
        pref.entryValues = blackImageRemovalValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.BlankImageRemovalMode.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_COLOR_DROPOUT_MODE) as ListPreference
        val colorDropoutModeEntries = ArrayList<CharSequence>()
        val colorDropoutModeValues = ArrayList<CharSequence>()

        for (colorDropoutMode in caps!!.colorDropoutModeList) {
            colorDropoutModeEntries.add(colorDropoutMode.name) //name
            colorDropoutModeValues.add(colorDropoutMode.name) //value
        }
        pref.entries = colorDropoutModeEntries.toTypedArray()
        pref.entryValues = colorDropoutModeValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.ColorDropoutMode.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_CROP_MODE) as ListPreference
        val cropModeEntries = ArrayList<CharSequence>()
        val cropModeValues = ArrayList<CharSequence>()

        for (cropMode in caps!!.cropModeList) {
            cropModeEntries.add(cropMode.name) //name
            cropModeValues.add(cropMode.name) //value
        }
        pref.entries = cropModeEntries.toTypedArray()
        pref.entryValues = cropModeValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.CropMode.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        // Load Original Size
        pref = findPreference(PREF_ORG_SIZE) as ListPreference
        val osEntries = ArrayList<CharSequence>()
        val osEntryValues = ArrayList<CharSequence>()

        for (os in caps!!.scanSizeList) {
            osEntries.add(os.name)
            osEntryValues.add(os.name)
        }

        pref.entries = osEntries.toTypedArray()
        pref.entryValues = osEntryValues.toTypedArray()
        pref.setValueIndex(0)
        pref.summary = "%s"

        // ME is always presented
        pref.setDefaultValue(caps!!.destinationList[0])
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_SCAN_PREVIEW) as ListPreference
        val scanPreviewEntries = ArrayList<CharSequence>()
        val scanPreviewEntryValues = ArrayList<CharSequence>()

        for (scanPreview in caps!!.scanPreviewList) {
            scanPreviewEntries.add(scanPreview.name)
            scanPreviewEntryValues.add(scanPreview.name)
        }
        pref.entries = scanPreviewEntries.toTypedArray()
        pref.entryValues = scanPreviewEntryValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.ScanPreview.DEFAULT)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_OUTPUT_QUALITY) as ListPreference
        val outputQualityEntries = ArrayList<CharSequence>()
        val outputQualityValues = ArrayList<CharSequence>()

        for (outputQuality in caps!!.outputQualityList) {
            outputQualityEntries.add(outputQuality.name) //name
            outputQualityValues.add(outputQuality.name) //value
        }
        pref.entries = outputQualityEntries.toTypedArray()
        pref.entryValues = outputQualityValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.OutputQuality.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_SHARPNESS_ADJUSTMENT) as ListPreference
        val sharpnessAdjustmentEntries = ArrayList<CharSequence>()
        val sharpnessAdjustmentValues = ArrayList<CharSequence>()

        for (sharpnessAdjustment in caps!!.sharpnessAdjustmentList) {
            sharpnessAdjustmentEntries.add(sharpnessAdjustment.name) //name
            sharpnessAdjustmentValues.add(sharpnessAdjustment.name) //value
        }
        pref.entries = sharpnessAdjustmentEntries.toTypedArray()
        pref.entryValues = sharpnessAdjustmentValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.SharpnessAdjustment.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_MEDIA_WEIGHT_ADJUSTMENT) as ListPreference
        val mediaWeightAdjustmentEntries = ArrayList<CharSequence>()
        val mediaWeightAdjustmentValues = ArrayList<CharSequence>()

        for (mediaWeightAdjustment in caps!!.mediaWeightAdjustmentList) {
            mediaWeightAdjustmentEntries.add(mediaWeightAdjustment.name) //name
            mediaWeightAdjustmentValues.add(mediaWeightAdjustment.name) //value
        }
        pref.entries = mediaWeightAdjustmentEntries.toTypedArray()
        pref.entryValues = mediaWeightAdjustmentValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.MediaWeightAdjustment.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_TEXT_PHOTO_OPTIMIZATION) as ListPreference
        val textPhotoOptimizationEntries = ArrayList<CharSequence>()
        val textPhotoOptimizationValues = ArrayList<CharSequence>()

        for (textPhotoOptimization in caps!!.textPhotoOptimizationList) {
            textPhotoOptimizationEntries.add(textPhotoOptimization.name) //name
            textPhotoOptimizationValues.add(textPhotoOptimization.name) //value
        }
        pref.entries = textPhotoOptimizationEntries.toTypedArray()
        pref.entryValues = textPhotoOptimizationValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.TextPhotoOptimization.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_MEDIA_SOURCE) as ListPreference
        val mediaSourceEntries = ArrayList<CharSequence>()
        val mediaSourceValues = ArrayList<CharSequence>()

        for (mediaSource in caps!!.mediaSourceList) {
            mediaSourceEntries.add(mediaSource.name) //name
            mediaSourceValues.add(mediaSource.name) //value
        }
        pref.entries = mediaSourceEntries.toTypedArray()
        pref.entryValues = mediaSourceValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.MediaSource.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        pref = findPreference(PREF_MISFEED_DETECTION_MODE) as ListPreference
        val misfeedDetectionModeEntries = ArrayList<CharSequence>()
        val misfeedDetectionModeValues = ArrayList<CharSequence>()

        for (misfeedDetectionMode in caps!!.misfeedDetectionModeList) {
            misfeedDetectionModeEntries.add(misfeedDetectionMode.name) //name
            misfeedDetectionModeValues.add(misfeedDetectionMode.name) //value
        }
        pref.entries = misfeedDetectionModeEntries.toTypedArray()
        pref.entryValues = misfeedDetectionModeValues.toTypedArray()
        pref.setDefaultValue(ScanAttributes.MisfeedDetectionMode.DEFAULT.name)
        pref.setValueIndex(0)
        pref.summary = "%s"

        mCustomLengthPref!!.setLimits(caps!!.customLengthRange.lowerBound, caps!!.customLengthRange.upperBound)
        mCustomWidthPref!!.setLimits(caps!!.customWidthRange.lowerBound, caps!!.customWidthRange.upperBound)

        // Doc Formats for different destinations
        fillDocFormatPreferences(caps, PREF_DOC_FORMAT, ScanAttributes.Destination.ME)
    }

    private fun fillDocFormatPreferences(
        caps: ScanAttributesCaps?,
        prefDocFormat: String,
        dest: ScanAttributes.Destination
    ) {
        if (caps == null) {
            return
        }

        val pref = findPreference(prefDocFormat) as ListPreference

        val dfEntries = ArrayList<CharSequence>()
        val dfEntryValues = ArrayList<CharSequence>()

        // Load Doc format
        for (df in caps!!.getDocumentFormatList(dest)) {
            dfEntries.add(df.name)
            dfEntryValues.add(df.name)
        }

        if (pref != null) {
            pref!!.entries = dfEntries.toTypedArray()
            pref!!.entryValues = dfEntryValues.toTypedArray()
            pref!!.setValueIndex(0)
            pref!!.summary = "%s"
        }
    }

    private fun setColorMode(sharedPreferences: SharedPreferences, key: String) {
        val docFormat = DocumentFormat.valueOf(sharedPreferences.getString(key, DocumentFormat.DEFAULT.name))
        val colorModeList = findPreference(ScanConfigureFragment.PREF_COLOR_MODE) as ListPreference

        if (caps != null) {
            val entries = ArrayList<CharSequence>()
            entries.add(ColorMode.DEFAULT.name)

            for (entry in caps!!.documentFormatsByColorMode.entries) {
                if ((entry.value.contains(docFormat) && entry.key != ColorMode.DEFAULT)) {
                    entries.add(entry.key.name)
                }
            }

            val entriesArray = entries.toTypedArray()
            colorModeList.entries = entriesArray
            colorModeList.entryValues = entriesArray
            colorModeList.setValueIndex(0)
        } else {
            colorModeList.setEntries(R.array.pref_default_entries)
            colorModeList.setEntryValues(R.array.pref_default_entries)
        }
    }

    private fun showOptionalPreference(preferences: SharedPreferences, key: String) {
        mBaseAttributesCategory!!.removePreference(mPDFCompressionPref)
        mBaseAttributesCategory!!.removePreference(mOCRLanguagePref)
        mBaseAttributesCategory!!.removePreference(mPDFPasswordPref)
        mBaseAttributesCategory!!.removePreference(mTIFFCompressionPref)
        mBaseAttributesCategory!!.removePreference(mXPSCompressionPref)

        val docFormat = DocumentFormat.valueOf(preferences.getString(key, DocumentFormat.DEFAULT.name))
        when (docFormat) {
            ScanAttributes.DocumentFormat.PDF -> {
                mBaseAttributesCategory!!.addPreference(mPDFCompressionPref)
                mBaseAttributesCategory!!.addPreference(mPDFPasswordPref)
            }
            ScanAttributes.DocumentFormat.OCR_PDF_TEXT_UNDER_IMAGE -> {
                mBaseAttributesCategory!!.addPreference(mPDFCompressionPref)
                mBaseAttributesCategory!!.addPreference(mOCRLanguagePref)
                mBaseAttributesCategory!!.addPreference(mPDFPasswordPref)
            }
            ScanAttributes.DocumentFormat.MTIFF, ScanAttributes.DocumentFormat.TIFF -> mBaseAttributesCategory!!.addPreference(
                mTIFFCompressionPref
            )
            ScanAttributes.DocumentFormat.OCR_PDF_A_TEXT_UNDER_IMAGE -> {
                mBaseAttributesCategory!!.addPreference(mPDFCompressionPref)
                mBaseAttributesCategory!!.addPreference(mOCRLanguagePref)
            }
            ScanAttributes.DocumentFormat.OCR_CSV, ScanAttributes.DocumentFormat.OCR_HTML, ScanAttributes.DocumentFormat.OCR_RTF, ScanAttributes.DocumentFormat.OCR_TEXT, ScanAttributes.DocumentFormat.OCR_UNICODE_TEXT -> mBaseAttributesCategory!!.addPreference(
                mOCRLanguagePref
            )
            ScanAttributes.DocumentFormat.PDF_A -> mBaseAttributesCategory!!.addPreference(mPDFCompressionPref)
            ScanAttributes.DocumentFormat.XPS -> mBaseAttributesCategory!!.addPreference(mXPSCompressionPref)
        }
    }

    private fun showCustomSizePreference(preferences: SharedPreferences, key: String) {
        val scanSize = ScanSize.valueOf(preferences.getString(key, ScanSize.DEFAULT.name))

        if (scanSize == ScanSize.CUSTOM) {
            mBaseAttributesCategory!!.addPreference(mCustomLengthPref)
            mBaseAttributesCategory!!.addPreference(mCustomWidthPref)
        } else {
            mBaseAttributesCategory!!.removePreference(mCustomLengthPref)
            mBaseAttributesCategory!!.removePreference(mCustomWidthPref)
        }
    }

    private fun supportTransmissionModeCaps(isSupported: Boolean) {
        if (isSupported) {
            mBaseAttributesCategory!!.addPreference(mTransmissionPref)
            if (mTransmissionPref != null) {
                val transmissionModeEntries = ArrayList<CharSequence>()
                val transmissionModeValues = ArrayList<CharSequence>()

                for (transmissionMode in caps!!.transmissionModeList) {
                    transmissionModeEntries.add(transmissionMode.name) //name
                    transmissionModeValues.add(transmissionMode.name) //value
                }
                mTransmissionPref!!.entries = transmissionModeEntries.toTypedArray()
                mTransmissionPref!!.entryValues = transmissionModeValues.toTypedArray()
                mTransmissionPref!!.setDefaultValue(ScanAttributes.TransmissionMode.DEFAULT.name)
                mTransmissionPref!!.setValueIndex(0)
                mTransmissionPref!!.summary = "%s"
            }
        } else {
            mBaseAttributesCategory!!.removePreference(mTransmissionPref)
            val mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().applicationContext)
            val editor = mPrefs.edit()
            editor.putString(PREF_TRANSMISSION_MODE, ScanAttributes.TransmissionMode.DEFAULT.name)
            editor.commit()
        }
    }

    private fun fillFileOptionAttrCaps() {
        if (isSDKInitialized) {
            val docPref = findPreference(PREF_DOC_FORMAT) as ListPreference
            val docEntry = docPref.entry as String
            val docFormat =
                if (docEntry == null) DocumentFormat.DEFAULT else ScanAttributes.DocumentFormat.valueOf(docEntry)

            val colorPref = findPreference(PREF_COLOR_MODE) as ListPreference
            val colorEntry = colorPref.entry as String
            val colorMode = if (colorEntry == null) ColorMode.DEFAULT else ScanAttributes.ColorMode.valueOf(colorEntry)

            val mPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity().applicationContext)

            val fileOptionsAttrCaps = requestFileOptionsCapabilities(colorMode, docFormat)

            var editPref = findPreference(PREF_PDF_PASSWORD) as? EditTextPreference
            val isPdfEncryptionSupport = fileOptionsAttrCaps!!.isPdfEncryptionPasswordSupported
            if (editPref != null && isPdfEncryptionSupport) {
                val password = mPrefs.getString(PREF_PDF_PASSWORD, null)
                editPref!!.text = password
            } else {
                val editor = mPrefs.edit()
                editor.putString(PREF_PDF_PASSWORD, null)
                editor.apply()
            }

            var pref: ListPreference? = findPreference(PREF_OCR_LANGUAGE) as ListPreference?

            val ocrEntries = ArrayList<CharSequence>()
            val ocrEntryValues = ArrayList<CharSequence>()

            for (language in fileOptionsAttrCaps!!.ocrLanguageList) {
                if (language != null && language!!.name != null) {
                    ocrEntries.add(language!!.name)
                    ocrEntryValues.add(language!!.name)
                }
            }

            if (pref != null) {
                pref!!.entries = ocrEntries.toTypedArray()
                pref!!.entryValues = ocrEntryValues.toTypedArray()
                pref!!.setValueIndex(0)
                pref!!.summary = "%s"
            } else {
                val editor = mPrefs.edit()
                editor.putString(PREF_OCR_LANGUAGE, FileOptionsAttributes.OcrLanguage.DEFAULT.name)
                editor.commit()
            }

            pref = findPreference(PREF_PDF_COMPRESSION) as ListPreference?

            val pdfCompEntries = ArrayList<CharSequence>()
            val pdfCompEntryValues = ArrayList<CharSequence>()

            for (compressionMode in fileOptionsAttrCaps!!.pdfCompressionModeList) {
                pdfCompEntries.add(compressionMode.name)
                pdfCompEntryValues.add(compressionMode.name)
            }

            if (pref != null) {
                pref!!.entries = pdfCompEntries.toTypedArray()
                pref!!.entryValues = pdfCompEntryValues.toTypedArray()
                pref!!.setValueIndex(0)
                pref!!.summary = "%s"
            } else {
                val editor = mPrefs.edit()
                editor.putString(PREF_PDF_COMPRESSION, FileOptionsAttributes.PdfCompressionMode.DEFAULT.name)
                editor.commit()
            }

            pref = findPreference(PREF_TIFF_COMPRESSION) as ListPreference?

            val tiffCompEntries = ArrayList<CharSequence>()
            val tiffCompEntryValues = ArrayList<CharSequence>()

            for (tiffComp in fileOptionsAttrCaps!!.tiffCompressionModeList) {
                tiffCompEntries.add(tiffComp.name)
                tiffCompEntryValues.add(tiffComp.name)
            }

            if (pref != null) {
                pref!!.entries = tiffCompEntries.toTypedArray()
                pref!!.entryValues = tiffCompEntryValues.toTypedArray()
                pref!!.setValueIndex(0)
                pref!!.summary = "%s"
            } else {
                val editor = mPrefs.edit()
                editor.putString(PREF_TIFF_COMPRESSION, FileOptionsAttributes.TiffCompressionMode.DEFAULT.name)
                editor.commit()
            }

            pref = findPreference(PREF_XPS_COMPRESSION) as ListPreference?

            val xpsCompEntries = ArrayList<CharSequence>()
            val xpsCompEntryValues = ArrayList<CharSequence>()

            for (xpsComp in fileOptionsAttrCaps!!.xpsCompressionModeList) {
                xpsCompEntries.add(xpsComp.name)
                xpsCompEntryValues.add(xpsComp.name)
            }

            if (pref != null) {
                pref!!.entries = xpsCompEntries.toTypedArray()
                pref!!.entryValues = xpsCompEntryValues.toTypedArray()
                pref!!.setValueIndex(0)
                pref!!.summary = "%s"
            } else {
                val editor = mPrefs.edit()
                editor.putString(PREF_XPS_COMPRESSION, FileOptionsAttributes.XpsCompressionMode.DEFAULT.name)
                editor.commit()
            }
        }
    }
}

