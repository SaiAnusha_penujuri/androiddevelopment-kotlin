package com.example.saianushapenujuri.scan_kotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.hp.jetadvantage.link.api.JetAdvantageLink
import com.hp.jetadvantage.link.api.SsdkUnsupportedException
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        try {
            JetAdvantageLink.getInstance().initialize(this)
        } catch (e: SsdkUnsupportedException) {
            Log.e(localClassName, "SDK is not supported!", e)
            e.printStackTrace()
        } catch (e: SecurityException) {
            Log.e(localClassName, "Security exception!", e)
            e.printStackTrace()
        }
        fragmentManager.beginTransaction()
            .replace(R.id.frameFragment, ScanConfigureFragment()).commit()
    }
}
