package com.example.saianushapenujuri.scan_kotlin;

import android.content.Context;
import android.os.Environment;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

public final class FolderPathPreference extends EditTextPreference implements CompoundButton.OnCheckedChangeListener {

    private static final String DEFAULT_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/Scan";

    RadioButton rbtnDownloadFolder;
    RadioButton rbtnExternalStorage;
    EditText edtFolderPath;

    String folderPath = DEFAULT_PATH;

    public FolderPathPreference(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public FolderPathPreference(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FolderPathPreference(final Context context) {
        super(context);
        init();
    }

    private void init() {
        setDialogLayoutResource(R.layout.preference_folder_path);
    }

    @Override
    protected void onBindDialogView(final View view) {
        super.onBindDialogView(view);
        findViewElements(view);
    }

    private void findViewElements(View view){
        rbtnDownloadFolder = view.findViewById(R.id.rbtn_download_storage);
        rbtnExternalStorage = view.findViewById(R.id.rbtn_ext_storage);
        rbtnDownloadFolder.setOnCheckedChangeListener(this);
        rbtnExternalStorage.setOnCheckedChangeListener(this);

        folderPath = getSharedPreferences().getString(getKey(), DEFAULT_PATH);
        edtFolderPath = view.findViewById(R.id.edt_folder_path);
        String path = getText();
        String externalStorage = Environment.getExternalStorageDirectory().getPath();
        String downloadFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();

        if (path.startsWith(downloadFolder)) {
            rbtnDownloadFolder.setChecked(true);
            path = path.replaceFirst(downloadFolder, "");
        } else {
            rbtnExternalStorage.setChecked(true);
            path = path.replaceFirst(externalStorage, "");
        }

        edtFolderPath.setText(path);
        edtFolderPath.setSelection(path.length());
    }

    @Override
    protected void onDialogClosed(final boolean positiveResult) {
        super.onDialogClosed(positiveResult);

        if (positiveResult) {
            setSummary(folderPath);
        }
    }

    @Override
    public String getText() {
        return folderPath != null ? folderPath : null;
    }

    @Override
    public void setText(final String text) {
        final boolean wasBlocking = shouldDisableDependents();

        if (edtFolderPath != null) {
            String path = edtFolderPath.getText().toString();
            if (!path.startsWith("/")) {
                path = "/" + path;
            }

            if (rbtnExternalStorage != null && rbtnExternalStorage.isChecked()) {
                String externalStorage = Environment.getExternalStorageDirectory().getPath();
                folderPath = externalStorage + path;
            } else if (rbtnDownloadFolder != null && rbtnDownloadFolder.isChecked()) {
                String downloadFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
                folderPath = downloadFolder + path;
            }
        }

        persistString(folderPath != null ? folderPath : null);

        final boolean isBlocking = shouldDisableDependents();

        if (isBlocking != wasBlocking) {
            notifyDependencyChange(isBlocking);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

    }
}